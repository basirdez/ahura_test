<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class RegisterTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $random_email = "basirdez" . random_int(1000, 9999) . "@gmail.com";
            $browser->visit('https://beta:Rocket2021@jimble.com.au/user/registration/1')
                ->type('firstName', 'basir')
                ->type('lastName', 'test')
                ->type('email', $random_email)
                ->type('password', 'Mb123456')
                ->type('confirmPassword', 'Mb123456')
                ->screenshot('preview')
                ->press('Sign Up')
                ->waitForText('verification link', 8)
                ->assertPathIs("/user/registration/1.5")
                ->screenshot('result');
        });
    }
}
